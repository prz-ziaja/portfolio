# Portfolio
To view original certificate click on the image.
## 2021
### Apache Spark w praktyce
[<img src="https://gitlab.com/prz-ziaja/portfolio/-/raw/master/comarch_spark.png">](https://gitlab.com/prz-ziaja/portfolio/-/raw/master/comarch_spark.png)
### Accelerating End-to-End Data Science Workflows
[<img src="https://gitlab.com/prz-ziaja/portfolio/-/raw/master/accelerating_end-to-end-data-science-workflows.png">](https://courses.nvidia.com/certificates/dff7476cadf2451ca6908221584d613d)
### Deep Learning for Intelligent Video Analytics
[<img src="https://gitlab.com/prz-ziaja/portfolio/-/raw/master/nvidia_iva.png">](https://courses.nvidia.com/certificates/6a98fcb358ab49ab8399b9ddab50b631)
## 2020
### Fundamentals of Deep Learning by NVIDIA Deep Learning Institute
[<img src="https://gitlab.com/prz-ziaja/portfolio/-/raw/master/nvidia_fundamentals.png">](https://courses.nvidia.com/certificates/8b35c9d986754ab3b93057cf4c22f5a3)
### Deep Learning by DeepLearning.AI
[<img src="https://gitlab.com/prz-ziaja/portfolio/-/raw/master/deep_learningAI.png">](https://coursera.org/share/7b9976ccb85f2cdb06679fe6a0def5d3)
### Advanced Data Science with IBM by IBM
[<img src="https://gitlab.com/prz-ziaja/portfolio/-/raw/master/ibm.png">](https://coursera.org/share/f0ca7b20d22fbd3e8d571d9f25fca861)

## 2019
### Machine Learning by Stanford University (noncertified)
[<img src="https://gitlab.com/prz-ziaja/portfolio/-/raw/master/stanford.png">](https://www.coursera.org/learn/machine-learning)
